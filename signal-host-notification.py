#!/usr/bin/python3

import subprocess
from argparse import ArgumentParser
import socket

icingahost = socket.gethostname()

parser = ArgumentParser()
parser.add_argument("-a", "--author", dest="author", default=None)
parser.add_argument("-c", "--comment", dest="comment", default=None)
parser.add_argument("-d", "--displayname", dest="displayname", required=True)
parser.add_argument("-e", "--state", dest="state", required=True)
parser.add_argument("-g", "--group", dest="group", action="store_true")
parser.add_argument("-n", "--hostname", dest="failedhost", required=True)
parser.add_argument("-o", "--output", dest="hostoutput", required=True)
parser.add_argument("-r", "--recipient", dest="recipient", required=True)
parser.add_argument("-s", "--sender", dest="sender", required=True)
parser.add_argument("-t", "--type", dest="notificationtype", required=True)
parser.add_argument("-w", "--datetime", dest="datetime", required=True)
args = parser.parse_args()

text_to_send = """*** Monitoring on """+icingahost+"""***
"""+args.notificationtype+""" - """+args.displayname+""" is """+args.state+"""
Host: """+args.failedhost+"""
State: """+args.state+"""

Date/Time: """+args.datetime+"""

Additional Info: """+args.hostoutput+"""
"""

# comment & author are only set if a notification is submitted via Icingaweb2
# so only then the author name &  comment can & will be added to the text
if args.comment != None and args.author != None:
    text_to_send = text_to_send + "\n\n" + args.author + ": " + args.comment

# basic signal-cli command, yet without recipient
command_to_call = "signal-cli -u "+args.sender+" send -m \""+text_to_send+"\""

# "-g" must be added to the signal-cli command if the message should be sent
# to a signal group instead of a single user
if args.group == True:
    command_to_call = command_to_call + " -g " + args.recipient
else:
	command_to_call = command_to_call + " " + args.recipient

subprocess.call(command_to_call, shell=True)